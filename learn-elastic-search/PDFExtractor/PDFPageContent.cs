﻿using System;
using System.Collections.Generic;
using System.Text;
using UglyToad.PdfPig.Content;

namespace PDFExtractor
{
    public class PDFPageContent
    {
        public int PageNumber { get; set; }
        public string PageContent { get; set; }
        public IEnumerable<byte[]> PageImages { get; set; }
    }
}
