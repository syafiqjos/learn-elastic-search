﻿using Spire.Pdf;
using Spire.Pdf.Graphics;
using Spire.Pdf.Texts;
using System;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;

namespace PDFExtractor
{
    public class PDFHelperSpirePdf
    {
        public IEnumerable<PDFPageContent> ExtractPagesText(string pdfFilePath, bool truncateContent = true)
        {
            PdfDocument pdfDoc = new PdfDocument();
            pdfDoc.LoadFromFile(pdfFilePath);

            var pageContents = ExtractPagesText(pdfDoc, truncateContent);

            pdfDoc.Close();

            return pageContents;
        }

        public IEnumerable<PDFPageContent> ExtractPagesText(byte[] pdfFileBytes, bool truncateContent = true)
        {
            PdfDocument pdfDoc = new PdfDocument();
            pdfDoc.LoadFromBytes(pdfFileBytes);

            var pageContents = ExtractPagesText(pdfDoc, truncateContent);

            pdfDoc.Close();

            return pageContents;
        }

        public IEnumerable<PDFPageContent> ExtractPagesText(PdfDocument pdfDoc, bool truncateContent = true)
        {
            var pageContents = new List<PDFPageContent>();

            int pageCount = 1;
            foreach (PdfPageBase page in pdfDoc.Pages)
            {
                var extractedContent = ExtractPageText(page, truncateContent);

                pageContents.Add(new PDFPageContent
                {
                    PageNumber = pageCount,
                    PageContent = extractedContent,
                });

                pageCount++;
            }

            return pageContents;
        }

        public string ExtractPageText(PdfPageBase page, bool truncateContent = true)
        {
            var textExtractor = new PdfTextExtractor(page);
            var extractOptions = new PdfTextExtractOptions();
            extractOptions.IsExtractAllText = true;

            var extractedContent = textExtractor.ExtractText(extractOptions);

            if (truncateContent)
            {
                extractedContent = TruncateText(extractedContent);
            }

            return extractedContent;
        }

        public string TruncateText(string text)
        {
            var truncated = text
                        .Replace(" \r\n", string.Empty)
                        .Replace("\r\n", string.Empty)
                        .Trim();

            var reg = new Regex("([ \t]+[ \t]+)");
            truncated = reg.Replace(truncated, " ");

            return truncated;
        }
    }
}
