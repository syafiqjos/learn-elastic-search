﻿using iTextSharp.text.pdf;
using PdfToText;
using System;
using System.Collections.Generic;
using System.Text;

namespace PDFExtractor
{
    public class PDFParserExtendedIronPdf: PDFParser
    {
        public IEnumerable<PDFPageContent> ExtractPagesText(string pdfFilePath, bool truncateContent = false)
        {
            var pdfReader = new PdfReader(pdfFilePath);
            var pageCount = pdfReader.NumberOfPages;

            var contents = new List<PDFPageContent>();

            for (int i = 1; i <= pageCount; i++)
            {
                var pageContent = ExtractTextFromPDFBytes(pdfReader.GetPageContent(i));

                if (truncateContent)
                {
                    pageContent = pageContent.Replace(" \n\r", string.Empty).Replace("\n\r", string.Empty);
                    pageContent = pageContent.Trim();
                }

                contents.Add(new PDFPageContent
                {
                    PageNumber = i,
                    PageContent = pageContent
                });
            }

            return contents;
        }

        public IEnumerable<PDFPageContent> ExtractPagesText(byte[] pdfFileBytes, bool truncateContent = false)
        {
            var pdfReader = new PdfReader(pdfFileBytes);
            var pageCount = pdfReader.NumberOfPages;

            var contents = new List<PDFPageContent>();

            for (int i = 1; i <= pageCount; i++)
            {
                var pageContent = ExtractTextFromPDFBytes(pdfReader.GetPageContent(i));

                if (truncateContent)
                {
                    pageContent = pageContent.Replace(" \n\r", string.Empty).Replace("\n\r", string.Empty);
                }

                contents.Add(new PDFPageContent
                {
                    PageNumber = i,
                    PageContent = pageContent
                });
            }

            return contents;
        }
    }
}
