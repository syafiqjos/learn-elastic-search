﻿using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using UglyToad.PdfPig;
using UglyToad.PdfPig.Content;

namespace PDFExtractor
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // RunTesseract();
            // RunPDFHelperExtract();
            // RunPDFHelperWatermark();
            // RunPDFHelperWatermarkStream();
            RunFreeSpirePDF();

            // RunWatermarkBenchmark();

            // RunFreeSpireDoc();

            //RunIronPdfDoc2Pdf();
        }

        static void RunIronPdfDoc2Pdf()
        {
            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string wordPath = workingDirectory + "\\data\\suspend.docx";
            string pdfPath = workingDirectory + "\\data\\suspend.pdf";

            IronPdf.License.LicenseKey = "IRONSUITE.B41C3E65CDC046.FN.CASHBENTIES.COM.24684-218593AB7C-C7EKPUF-7PTM4FZZKUK4-SE5QKIRFLQOA-P7DNTDDTNQXF-T3BB6MLU3LUP-TP46BZH3TGUN-5HWCZ2XG4NNO-QXCHEY-TNSUWVNSIAGMEA-DEPLOYMENT.TRIAL-MBIB6M.TRIAL.EXPIRES.16.MAR.2024";

            IronPdf.DocxPdfRenderOptions options = new IronPdf.DocxPdfRenderOptions();

            IronPdf.DocxToPdfRenderer renderer = new IronPdf.DocxToPdfRenderer();
            // Render from DOCX file
            IronPdf.PdfDocument pdf = renderer.RenderDocxAsPdf(wordPath, options);
            // Save the PDF
            pdf.SaveAs(pdfPath);
        }

        static void RunFreeSpireDoc()
        {
            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string wordPath = workingDirectory + "\\data\\suspend.docx";
            string pdfPath = workingDirectory + "\\data\\suspend.pdf";

            var document = new Spire.Doc.Document();
            document.LoadFromFile(wordPath);

            //Save the document to PDF
            document.SaveToFile("suspend.pdf", Spire.Doc.FileFormat.PDF);

        }

        static void RunWatermarkBenchmark()
        {
            var repetition = 10;

            var filenames = new string[] { "TextOnly", "ImageOnly", "Youtube", "Youtube2" };
            var filesizes = new string[] { "3.6MB", "0.7MB", "75MB", "210MB" };
            var pages = new string[] { "736", "780", "15", "37" };

            for (int f = 0; f < filenames.Length; f++)
            {
                double totalTime = 0;

                Console.WriteLine($"Benchmark of {filenames[f]}.pdf with size {filesizes[f]} and {pages[f]} pages");

                for (int i = 0; i < repetition; i++)
                {
                    var initiateTime = DateTime.Now;

                    RunPDFHelperWatermark(filenames[f]);

                    var endTime = DateTime.Now;

                    var duration = (endTime - initiateTime).TotalMilliseconds;
                    Console.WriteLine($"Iteration {i + 1} Duration time: {duration / 1000}s");

                    totalTime += duration;
                }

                Console.WriteLine($"Avg time: {totalTime / repetition / 1000}s");
                Console.WriteLine("==========");
            }
        }

        static void RunTesseract()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\magang-ocr.pdf";

            var pdfHelper = new PDFHelper();

            Console.WriteLine("FROM PDF FILE PATH");
            var pageContents = pdfHelper.ExtractPagesContent(pdfPath, true);

            foreach (var page in pageContents)
            {
                Console.WriteLine("PAGE: " + page.PageNumber);
                Console.WriteLine(page.PageContent);
                Console.WriteLine("\n");
            }

            Console.WriteLine("FROM PDF IMAGES");

            // string trainPath = workingDirectory + "\\data\\eng.traineddata";
            string trainPath = workingDirectory + "\\data";

            int imageId = 0;
            using (var engine = new Tesseract.TesseractEngine(trainPath, "ind", Tesseract.EngineMode.Default))
            {
                foreach (var page in pageContents)
                {
                    Console.WriteLine("PAGE: " + page.PageNumber);

                    foreach (var imageBytes in page.PageImages)
                    {
                        File.WriteAllBytes(workingDirectory + "\\data\\ocr-" + imageId++ + ".jpg", imageBytes);
                        using (var img = Tesseract.Pix.LoadFromMemory(imageBytes))
                        {
                            using (var extractedPage = engine.Process(img))
                            {
                                var txt = extractedPage.GetText();
                                Console.WriteLine(txt);
                                Console.WriteLine("\n");
                            }
                        }
                    }
                }
            }
        }

        static void RunPDFHelperExtract()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\magang.pdf";

            var pdfHelper = new PDFHelper();

            Console.WriteLine("FROM PDF FILE PATH");
            var pageContents = pdfHelper.ExtractPagesContent(pdfPath, true);

            foreach (var page in pageContents)
            {
                Console.WriteLine("PAGE: " + page.PageNumber);
                Console.WriteLine(page.PageContent);
                Console.WriteLine("\n");
            }
        }

        static void RunPDFHelperWatermark(string filename = null)
        {
            // Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\TextOnly.pdf";
            string pdfOutputPath = workingDirectory + "\\data\\TextOnly-watermark.pdf";

            if (filename != null)
            {
                pdfPath = workingDirectory + $"\\data\\{filename}.pdf";
                pdfOutputPath = workingDirectory + $"\\data\\{filename}-watermark.pdf";
            }

            var pdfHelper = new PDFHelper();

            // Console.WriteLine("WATERMARK PDF FILE PATH");
            // pdfHelper.WatermarkPages(pdfPath, pdfOutputPath, "Gajah");

            string watermarkSampleString = "Confidential";
            string watermarkIdentityString = $"Suitmedia - {DateTime.Now.ToString("yyyy-MM-dd")} - Admin";

            int watermarkSampleSize = 100;
            XFont watermarkSampleFont = new XFont("Times New Roman", watermarkSampleSize, XFontStyle.BoldItalic);

            int watermarkIdentitySize = 20;
            XFont watermarkIdentityFont = new XFont("Times New Roman", watermarkIdentitySize, XFontStyle.BoldItalic);

            pdfHelper.WatermarkPages(pdfPath, pdfOutputPath, (PdfSharp.Pdf.PdfPage page, XGraphics gfx) => {
                // Get the size (in points) of the text.
                var size = gfx.MeasureString(watermarkSampleString, watermarkSampleFont);
                var size2 = gfx.MeasureString(watermarkIdentityString, watermarkIdentityFont);

                // Define a rotation transformation at the center of the page.
                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                // Create a string format.
                var format = new XStringFormat();
                format.Alignment = XStringAlignment.Near;
                format.LineAlignment = XLineAlignment.Near;

                // Create a dimmed red brush.
                XBrush brush = new XSolidBrush(XColor.FromArgb(128, 255, 0, 0));

                // Draw the string.
                gfx.DrawString(watermarkSampleString, watermarkSampleFont, brush,
                    new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                    format);

                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(+Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);
                gfx.TranslateTransform(0, +page.Height / 2 - 20);

                XBrush brush2 = new XSolidBrush(XColor.FromArgb(255, 0, 0, 0));

                gfx.DrawString(watermarkIdentityString, watermarkIdentityFont, brush2,
                    new XPoint((page.Width - size2.Width) / 2, (page.Height - size2.Height) / 2),
                    format);
            });
        }

        static void RunPDFHelperWatermarkStream()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\bursed.pdf";
            string pdfOutputPath = workingDirectory + "\\data\\bursed-watermark-stream.pdf";

            var pdfHelper = new PDFHelper();

            Console.WriteLine("WATERMARK PDF FILE PATH");
            using (var stream = pdfHelper.WatermarkPagesStream(pdfPath, "Gajah"))
            {
                var bytes = pdfHelper.StreamToBytes(stream);

                File.WriteAllBytes(pdfOutputPath, bytes);
            }
        }

        static void RunPDFsharpWatermark()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\bursed.pdf";
            string plainTextPath = workingDirectory + "\\data\\bursed.txt";

            // Variation 1: Draw a watermark as a text string.
            var document = PdfReader.Open(pdfPath);
            string watermark = "Gajah";
            int emSize = 100;

            XFont font = new XFont("Times New Roman", emSize, XFontStyle.BoldItalic);

            for (int idx = 0; idx < document.Pages.Count; idx++)
            {
                var page = document.Pages[idx];

                // Get an XGraphics object for drawing beneath the existing content.
                var gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Append);

                // Get the size (in points) of the text.
                var size = gfx.MeasureString(watermark, font);

                // Define a rotation transformation at the center of the page.
                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                // Create a string format.
                var format = new XStringFormat();
                format.Alignment = XStringAlignment.Near;
                format.LineAlignment = XLineAlignment.Near;

                // Create a dimmed red brush.
                XBrush brush = new XSolidBrush(XColor.FromArgb(128, 255, 0, 0));

                // Draw the string.
                gfx.DrawString(watermark, font, brush,
                    new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                    format);

                document.Save(workingDirectory + "\\data\\bursed-watermark.pdf");
            }
        }

        static void RunPdfPigExtract()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\bursed.pdf";
            string plainTextPath = workingDirectory + "\\data\\bursed.txt";

            using (PdfDocument document = PdfDocument.Open(pdfPath))
            {
                foreach (Page page in document.GetPages())
                {
                    Console.WriteLine("PAGE NUMBER: " + page.Number);
                    string pageText = page.Text;

                    Console.WriteLine(pageText);

                    /*
                    foreach (Word word in page.GetWords())
                    {
                        Console.Write(word.Text + " ");
                    }

                    Console.WriteLine();
                    */
                }
            }
        }

        /*
        static void RunFreeSpirePDFWatermark()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\bursed.pdf";
            string plainTextPath = workingDirectory + "\\data\\bursed.txt";

            //Create a PdfDocument object
            PdfDocument pdf = new PdfDocument();

            //Load a sample PDF document
            pdf.LoadFromFile(pdfPath);

            //Create a PdfTrueTypeFont object
            PdfTrueTypeFont font = new PdfTrueTypeFont(new Font("Arial", 50f), true);

            //Set the watermark text
            string text = "CONFIDENTIAL";

            //Measure the text size
            SizeF textSize = font.MeasureString(text);

            //Calculate the values of two offset variables,
            //which will be used to calculate the translation amount of the coordinate system
            float offset1 = (float)(textSize.Width * System.Math.Sqrt(2) / 4);
            float offset2 = (float)(textSize.Height * System.Math.Sqrt(2) / 4);

            //Traverse all the pages in the document
            foreach (PdfPageBase page in pdf.Pages)
            {
                //Set the page transparency
                page.Canvas.SetTransparency(0.8f);

                //Translate the coordinate system by specified coordinates
                page.Canvas.TranslateTransform(page.Canvas.Size.Width / 2 - offset1 - offset2, page.Canvas.Size.Height / 2 + offset1 - offset2);

                //Rotate the coordinate system 45 degrees counterclockwise
                page.Canvas.RotateTransform(-45);

                //Draw watermark text on the page
                page.Canvas.DrawString(text, font, PdfBrushes.DarkGray, 0, 0);
            }

            //Save the changes to another file
            pdf.SaveToFile(workingDirectory + "\\data\\bursed-watermark.pdf");
        }
        */

        static void RunFreeSpirePDF()
        {
            Console.WriteLine("Hello World!");

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\magang-ocr.pdf";

            var pdfHelper = new PDFHelperSpirePdf();
            var pageContents = pdfHelper.ExtractPagesText(pdfPath, true);

            Console.WriteLine("FROM PDF FILE PATH");
            foreach (var page in pageContents)
            {
                Console.WriteLine("PAGE: " + page.PageNumber);
                Console.WriteLine(page.PageContent);
                Console.WriteLine("\n");
            }
        }

        static void RunPDFParser()
        {
            Console.WriteLine("Hello World!");

            var pdfParser = new PDFParserExtended();

            string workingDirectory = Directory.GetParent(Environment.CurrentDirectory).Parent.Parent.FullName;
            string pdfPath = workingDirectory + "\\data\\bursed.pdf";
            string plainTextPath = workingDirectory + "\\data\\bursed.txt";

            pdfParser.ExtractText(pdfPath, plainTextPath);

            var ironPdfDoc = IronPdf.PdfDocument.FromFile(pdfPath);

            // string ironPdfDocContent = ironPdfDoc.ExtractAllText();

            var pdfBytes = File.ReadAllBytes(pdfPath);

            var pageContentsFromFilePath = pdfParser.ExtractPagesText(pdfPath, true).ToList();
            var pageContentsFromBytes = pdfParser.ExtractPagesText(pdfBytes, true).ToList();

            Console.WriteLine("FROM PDF FILE PATH");
            foreach (var page in pageContentsFromFilePath)
            {
                Console.WriteLine("PAGE: " + page.PageNumber);
                Console.WriteLine(page.PageContent);
                Console.WriteLine("\n");
            }

            Console.WriteLine("FROM PDF BYTES");
            foreach (var page in pageContentsFromBytes)
            {
                Console.WriteLine("PAGE: " + page.PageNumber);
                Console.WriteLine(page.PageContent);
                Console.WriteLine("\n");
            }
        }
    }
}
