﻿using PdfSharp.Drawing;
using PdfSharp.Pdf.IO;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using UglyToad.PdfPig;
using UglyToad.PdfPig.Content;

namespace PDFExtractor
{
    public class PDFHelper
    {
        public PDFHelper()
        {
            // Penting, agar PDFsharp bisa watermark
            Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);
        }

        // TODO: Merge PDFs
        // TODO: Read OCR of PDFs page

        public IEnumerable<PDFPageContent> ExtractPagesContent(string pdfFilePath, bool truncateContent = true)
        {
            using (PdfDocument document = PdfDocument.Open(pdfFilePath))
            {
                return ExtractPagesContent(document, truncateContent);
            }
        }

        public IEnumerable<PDFPageContent> ExtractPagesContent(byte[] pdfFileBytes, bool truncateContent = true)
        {
            using (PdfDocument document = PdfDocument.Open(pdfFileBytes))
            {
                return ExtractPagesContent(document, truncateContent);
            }
        }

        public IEnumerable<PDFPageContent> ExtractPagesContent(PdfDocument pdfDoc, bool truncateContent = true)
        {
            var pageContents = new List<PDFPageContent>();

            int pageCount = 1;
            foreach (Page page in pdfDoc.GetPages())
            {
                var extractedContent = page.Text;
                var extractedImages = new List<byte[]>();

                foreach (var image in page.GetImages())
                {
                    byte[] bytes;
                    if (image.TryGetPng(out bytes))
                    {
                        extractedImages.Add(bytes);
                    }
                    else if (image.TryGetBytes(out var b))
                    {
                        bytes = b.ToArray();
                        extractedImages.Add(bytes.ToArray());
                    } else
                    {
                        bytes = image.RawBytes.ToArray();
                        extractedImages.Add(bytes);
                    }
                    // extractedImages.Add(image.RawBytes.ToArray());
                }

                if (truncateContent)
                {
                    extractedContent = TruncateText(extractedContent);
                }

                pageContents.Add(new PDFPageContent
                {
                    PageNumber = pageCount,
                    PageContent = extractedContent,
                    PageImages = extractedImages
                });

                pageCount++;
            }

            return pageContents;
        }

        public string TruncateText(string text)
        {
            var truncated = text
                        .Replace(" \r\n", string.Empty)
                        .Replace("\r\n", string.Empty)
                        .Trim();

            var reg = new Regex("([ \t]+[ \t]+)");
            truncated = reg.Replace(truncated, " ");

            return truncated;
        }

        public void WatermarkPages(string pdfPath, string pdfOutputPath, string watermark)
        {
            var document = WatermarkPages(pdfPath, watermark);

            document.Save(pdfOutputPath);
            document.Close();
        }

        public void WatermarkPages(string pdfPath, string pdfOutputPath, Action<PdfSharp.Pdf.PdfPage, XGraphics> draw)
        {
            var document = WatermarkPages(pdfPath, draw);

            document.Save(pdfOutputPath);
            document.Close();
        }

        private PdfSharp.Pdf.PdfDocument WatermarkPages(string pdfPath, string watermark)
        {
            int emSize = 100;
            XFont font = new XFont("Times New Roman", emSize, XFontStyle.BoldItalic);

            var document = WatermarkPages(pdfPath, (page, gfx) =>
            {
                // Get the size (in points) of the text.
                var size = gfx.MeasureString(watermark, font);

                // Define a rotation transformation at the center of the page.
                gfx.TranslateTransform(page.Width / 2, page.Height / 2);
                gfx.RotateTransform(-Math.Atan(page.Height / page.Width) * 180 / Math.PI);
                gfx.TranslateTransform(-page.Width / 2, -page.Height / 2);

                // Create a string format.
                var format = new XStringFormat();
                format.Alignment = XStringAlignment.Near;
                format.LineAlignment = XLineAlignment.Near;

                // Create a dimmed red brush.
                XBrush brush = new XSolidBrush(XColor.FromArgb(128, 255, 0, 0));

                // Draw the string.
                gfx.DrawString(watermark, font, brush,
                    new XPoint((page.Width - size.Width) / 2, (page.Height - size.Height) / 2),
                    format);
            });

            return document;
        }

        private PdfSharp.Pdf.PdfDocument WatermarkPages(string pdfPath, Action<PdfSharp.Pdf.PdfPage, XGraphics> draw)
        {
            var document = PdfReader.Open(pdfPath);

            for (int idx = 0; idx < document.Pages.Count; idx++)
            {
                var page = document.Pages[idx];

                // Get an XGraphics object for drawing beneath the existing content.
                var gfx = XGraphics.FromPdfPage(page, XGraphicsPdfPageOptions.Append);

                draw.Invoke(page, gfx);
            }

            return document;
        }

        public Stream WatermarkPagesStream(string pdfPath, string watermark)
        {
            var document = WatermarkPages(pdfPath, watermark);

            var memoryStream = new MemoryStream();
            document.Save(memoryStream);
            document.Clone();

            return memoryStream;
        }

        public Stream WatermarkPagesStream(string pdfPath, Action<PdfSharp.Pdf.PdfPage, XGraphics> draw)
        {
            var document = WatermarkPages(pdfPath, draw);

            var memoryStream = new MemoryStream();
            document.Save(memoryStream);
            document.Clone();

            return memoryStream;
        }

        public byte[] StreamToBytes(Stream stream)
        {
            using (var binaryReader = new BinaryReader(stream))
            {
                var bytes = binaryReader.ReadBytes((int)stream.Length);
                return bytes;
            }
        }
    }
}
