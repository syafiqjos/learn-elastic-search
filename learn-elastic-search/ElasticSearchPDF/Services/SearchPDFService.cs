﻿using Elastic.Clients.Elasticsearch;
using ElasticSearchPDF.Models;
using System.Collections.Generic;
using System.IO;
using System;
using System.Threading.Tasks;

namespace ElasticSearchPDF.Services
{
    public class SearchPDFService : ISearchPDFService
    {
        private readonly ElasticsearchClient _elasticsearch;
        private readonly string indexName = "pdf_files";

        public SearchPDFService(ElasticsearchClient elasticsearch)
        {
            _elasticsearch = elasticsearch;
        }

        public async Task<IEnumerable<SearchData>> SearchPDFByContent(string match)
        {
            var response = await _elasticsearch.SearchAsync<SearchData>(s => s
                .Index(indexName)
                .From(0)
                .Size(10)
                .Query(q => q
                    .Match(m => m
                        .Field(p => p.PageContent)
                        .Query(match)
                    )
                )
            );

            if (!response.IsValidResponse)
            {
                throw new ApplicationException("Search PDF By Content fail");
            }

            return response.Documents;
        }

        public async Task<IEnumerable<SearchData>> SearchPDFByTitle(string match)
        {
            var response = await _elasticsearch.SearchAsync<SearchData>(s => s
                .Index(indexName)
                .From(0)
                .Size(10)
                .Query(q => q
                    .Match(m => m
                        .Field(p => p.FileName)
                        .Query(match)
                    )
                )
            );

            if (!response.IsValidResponse)
            {
                throw new ApplicationException("Search PDF By Title fail");
            }

            return response.Documents;
        }

        public IEnumerable<string> ScanDirectoryGetPDFs(string dirPath)
        {
            var files = Directory.GetFiles(dirPath);

            var list = new List<string>();

            foreach (var filePath in files) {
                if (Path.GetExtension(filePath) == ".pdf")
                {
                    list.Add(filePath);
                }
            }

            return list;
        }

        public async Task StorePDFContentPage(string filePath, int pageNumber, string pageContent)
        {
            var doc = new SearchData
            {
                FilePath = filePath,
                FileUrl = filePath,
                PageNumber = pageNumber,
                PageContent = pageContent
            };

            var response = await _elasticsearch.IndexAsync(doc, indexName);

            if (!response.IsValidResponse)
            {
                throw new ApplicationException("Storing to Elasticsearch fail");
            }
        }
    }
}
