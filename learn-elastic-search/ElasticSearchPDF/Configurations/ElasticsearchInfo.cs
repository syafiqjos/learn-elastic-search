﻿namespace ElasticSearchPDF.Configurations
{
    public class ElasticsearchInfo
    {
        public string Host { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Fingerprint { get; set; }
    }
}
