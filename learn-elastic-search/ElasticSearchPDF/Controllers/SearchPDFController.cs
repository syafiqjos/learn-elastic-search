﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using PDFExtractor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ElasticSearchPDF.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class SearchPDFController : ControllerBase
    {
        private readonly ILogger<SearchPDFController> _logger;
        private readonly ISearchPDFService _searchPDFService;
        private readonly PDFParserExtended _pdfParser;

        public SearchPDFController(ILogger<SearchPDFController> logger, ISearchPDFService searchPDFService, PDFParserExtended pdfParser)
        {
            _logger = logger;
            _searchPDFService = searchPDFService;
            _pdfParser = pdfParser;
        }

        [HttpGet]
        public async Task<IActionResult> SearchPDFByTitle([FromQuery] string match)
        {
            var result = await _searchPDFService.SearchPDFByTitle(match);

            return Ok(new {
                data = result.ToList()
            });
        }

        [HttpGet]
        public async Task<IActionResult> SearchPDFByContent([FromQuery] string match)
        {
            var result = await _searchPDFService.SearchPDFByContent(match);

            return Ok(new
            {
                data = result.ToList()
            });
        }

        [HttpPost]
        public async Task<IActionResult> ScanDirectoryStorePDFContents([FromBody] string dirPath)
        {
            var result = _searchPDFService.ScanDirectoryGetPDFs(dirPath);

            foreach (var pdfPath in result)
            {
                var pages = _pdfParser.ExtractPagesText(pdfPath, true);
                foreach (var page in pages)
                {
                    await _searchPDFService.StorePDFContentPage(pdfPath, page.PageNumber, page.PageContent);
                }
            }

            return Ok(new
            {
                data = result.ToList()
            });
        }
    }
}
