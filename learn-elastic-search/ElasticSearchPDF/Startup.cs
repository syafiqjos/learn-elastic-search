using ElasticSearchPDF.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.OpenApi.Models;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Elastic.Clients.Elasticsearch;
using ElasticSearchPDF.Configurations;
using Elastic.Transport;
using PDFExtractor;

namespace ElasticSearchPDF
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddSwaggerGen(option =>
            {
                option.EnableAnnotations();
                option.SwaggerDoc("v1", new OpenApiInfo { Title = "BFF-Web-API", Version = "v1" });
            });

            services.AddScoped<PDFParserExtended>();
            services.AddScoped<ISearchPDFService, SearchPDFService>();
            services.AddScoped<ElasticsearchClient>(conf =>
            {
                var elasticsearchInfo = Configuration.GetSection(nameof(ElasticsearchInfo)).Get<ElasticsearchInfo>();

                var settings = new ElasticsearchClientSettings(new Uri(elasticsearchInfo.Host))
                    .CertificateFingerprint(elasticsearchInfo.Fingerprint)
                    .Authentication(new BasicAuthentication(elasticsearchInfo.Username, elasticsearchInfo.Password));

                return new ElasticsearchClient(settings);
            });

            services.AddControllers();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI();
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
