﻿using System.IO;

namespace ElasticSearchPDF.Models
{
    public class SearchData
    {
        public string FileUrl { get; set; }
        public string FilePath { get; set; }
        public string FileName { get => Path.GetFileName(FilePath); }
        public int PageNumber { get; set; }
        public string PageContent { get; set; }
    }
}
