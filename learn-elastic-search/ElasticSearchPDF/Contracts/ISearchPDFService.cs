﻿using ElasticSearchPDF.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace ElasticSearchPDF
{
    public interface ISearchPDFService
    {
        Task<IEnumerable<SearchData>> SearchPDFByTitle(string match);
        Task<IEnumerable<SearchData>> SearchPDFByContent(string match);
        IEnumerable<string> ScanDirectoryGetPDFs(string dirpath);
        Task StorePDFContentPage(string filePath, int pageNumber, string pageContent);
    }
}
