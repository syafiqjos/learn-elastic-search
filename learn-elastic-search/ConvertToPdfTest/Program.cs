﻿using Spire.Doc;
using Syncfusion.DocIO.DLS;
using Syncfusion.DocIO;
using Syncfusion.DocIORenderer;
using System.Diagnostics;
using Syncfusion.XlsIO;
using Syncfusion.XlsIORenderer;
using Syncfusion.Presentation;
using Syncfusion.PresentationRenderer;
using System.IO;

namespace ConvertToPdfTest
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Console.WriteLine("Hello, World!");

            //RunSpireConvertDocxToPdf();
            //RunSpireConvertExcelToPdf();
            //RunSpireConvertPresentationToPdf();

            //RunSyncFusionConvertDocxToPdf();
            //RunSyncFusionConvertExcelToPdf();
            RunSyncFusionConvertPresentationToPdf();
        }

        static void RunSpireConvertPresentationToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("pdfstuff.pptx");
            var outputFilePath = GetFilePathFromDataDir("pdfstuff2.pdf");

            //Create a Document object
            var document = new Spire.Presentation.Presentation();

            //Load a sample Word document
            document.LoadFromFile(inputFilePath);

            //Save the document to PDF
            document.SaveToFile(outputFilePath, Spire.Presentation.FileFormat.PDF);
        }

        static void RunSpireConvertExcelToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("plnip.xlsx");
            var outputFilePath = GetFilePathFromDataDir("plnip3.pdf");

            //Create a Document object
            var document = new Spire.Xls.Workbook();

            //Load a sample Word document
            document.LoadFromFile(inputFilePath);

            document.ConverterSetting.SheetFitToPage = true;

            //Save the document to PDF
            document.SaveToFile(outputFilePath, Spire.Xls.FileFormat.PDF);
        }

        static void RunSpireConvertDocxToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("suspend5.docx");
            var outputFilePath = GetFilePathFromDataDir("suspend5.pdf");

            //Create a Document object
            Document document = new Document();

            //Load a sample Word document
            document.LoadFromFile(inputFilePath);
  
            //Save the document to PDF
            document.SaveToFile(outputFilePath, FileFormat.PDF);
        }

        static void RunLibreOfficeConvertDocxToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("suspend5.docx");
            var outputFilePath = GetFilePathFromDataDir("");

            var process = new Process();
            process.StartInfo.FileName = @"C:\Program Files\LibreOffice\program\soffice.exe";
            process.StartInfo.Arguments = $"--convert-to pdf {inputFilePath} --outdir {outputFilePath}";
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;
            process.Start();

            var output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();
        }

        static void RunSyncFusionConvertDocxToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("suspend5.docx");
            var outputFilePath = GetFilePathFromDataDir("suspend5.pdf");

            var inputFileStream = File.Open(inputFilePath, FileMode.Open, FileAccess.Read);

            WordDocument wordDocument = new WordDocument(inputFileStream, Syncfusion.DocIO.FormatType.Docx);
            //Initializes the ChartToImageConverter for converting charts during Word to pdf conversion
            //wordDocument.ChartToImageConverter = new ChartToImageConverter();
            //Creates an instance of the DocToPDFConverter
            var converter = new Syncfusion.DocIORenderer.DocIORenderer();
            //Converts Word document into PDF document
            var pdfDocument = converter.ConvertToPDF(wordDocument);
            //Saves the PDF file

            using (var fileStream = File.Create(outputFilePath))
            {
                pdfDocument.Save(fileStream);
            }

            //Closes the instance of document objects
            pdfDocument.Close(true);
            wordDocument.Close();

            inputFileStream.Close();
        }

        static void RunSyncFusionConvertExcelToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("plnip.xlsx");
            var outputFilePath = GetFilePathFromDataDir("plnip4.pdf");

            var inputFileStream = File.Open(inputFilePath, FileMode.Open, FileAccess.Read);

            using (ExcelEngine excelEngine = new ExcelEngine())
            {
                IApplication application = excelEngine.Excel;
                application.DefaultVersion = ExcelVersion.Xlsx;
                IWorkbook workbook = application.Workbooks.Open(inputFileStream);

                //Initialize XlsIO renderer.
                XlsIORenderer renderer = new XlsIORenderer();

                //Convert Excel document into PDF document 
                var pdfDocument = renderer.ConvertToPDF(workbook);

                using (var fileStream = File.Create(outputFilePath))
                {
                    pdfDocument.Save(fileStream);
                }
            }

            inputFileStream.Close();
        }

        static void RunSyncFusionConvertPresentationToPdf()
        {
            var inputFilePath = GetFilePathFromDataDir("pdfstuff.pptx");
            var outputFilePath = GetFilePathFromDataDir("pdfstuff3.pdf");

            var inputFileStream = File.Open(inputFilePath, FileMode.Open, FileAccess.Read);

            //Open the existing PowerPoint presentation with loaded stream.
            using (IPresentation pptxDoc = Presentation.Open(inputFileStream))
            {
                //Convert the PowerPoint presentation to PDF document.
                using (var pdfDocument = PresentationToPdfConverter.Convert(pptxDoc))
                {
                    using (var output = File.Create(outputFilePath))
                    {
                        pdfDocument.Save(output);
                    }
                }
            }

            inputFileStream.Close();
        }

        static string GetFilePathFromDataDir(string path)
        {
            string dirPath = Environment.CurrentDirectory;

            return Path.Combine(dirPath, "../../../../PDFExtractor/data/" + path);
        }
    }
}
